<!DOCTYPE html>
<html>
<head>
	<title>Create</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		{!! Form::open(array('route' => 'course.store','class'=>'form-horizontal')) !!}
		{!! Form::token(); !!}
		<?php echo csfr_field(); ?>
            <div class="panel panel-default">
                <div class="panel-body form-horizontal payment-form">
                    <div class="form-group">
                        <label for="concept" class="col-sm-3 control-label">Course Title</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="concept" name="course_title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Course Code</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="description" name="course_code">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="amount" class="col-sm-3 control-label">Course Credite</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="amount" name="course_credite">
                        </div>
                    </div>
                </div>
            </div>            
        {!! Form::close() !!}
	</div>
</body>
</html>