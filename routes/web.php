<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   // return view('welcome');
    return "home";
});
Route::get('/about', function () {
   //return view('about');
    //return "about page";
});

Route::resource('course','CourseController');

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/alluser', function () {
    return view('user');
});

Route::get('alluser/{id}/{name}', function ($id,$name) {
    return 'user '.$id.$name;
});

Route::get('home','basicController@index');
Route::get('about','basicController@about');

/*the upper code is learn by Zakaria Parvez*/

/*the below code is learn by webslession*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/uploadfile', 'UploadfileController@index');
Route::post('/uploadfile', 'UploadfileController@upload');
Route::get('/main', 'MainController@index');
Route::post('/main/checklogin', 'MainController@checklogin');
Route::get('main/successlogin', 'MainController@successlogin');
Route::get('main/logout', 'MainController@logout');
Route::get('/new',function (){
    return "Hello Laravel";
});





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test',function(){

	return 'Welcome Laravel';
});
