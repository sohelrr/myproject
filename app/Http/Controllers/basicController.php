<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class basicController extends Controller
{
    public function index(){
        
        return "home from basic Controller";
    }
    public function about(){
        
        //return "about from basic Controller";
        $myname = "Sohel Rana";
        $id = "999999";
        $phone = "01729753365";
        $address = "Rajshahi";
        return view('about',compact('myname','id','phone'))->with('HomeTown',$address);
    }
    
}
